import endUrls from "./EndUrls";
import ApiClient from './index';

let singletonApiInstance = new ApiClient();

export const authenticateCancelClassToken = (
    queryParams = null,
    headers = null,
) => {
    const url = endUrls.authenticateCancelOTP;
    return singletonApiInstance.getRequest(url, queryParams, headers);
};

export const sendCancelVerificationCode = (
    queryParams = null,
    headers = null,
) => {
    const url = endUrls.sendCancleOTP;
    return singletonApiInstance.postRequest(url, queryParams, headers);
};

export const getOneTimeScheduler = (studentId, queryParams, headers = null) => {
    const url = endUrls.oneTimeScheduler(studentId);
    return singletonApiInstance.getRequest(url, queryParams, headers);
};

export const getAvaiableTeacherList = (
    studentId,
    queryParams,
    headers = null,
) => {
    const url = endUrls.avaiableTeacherList(studentId);
    return singletonApiInstance.getRequest(url, queryParams, headers);
};