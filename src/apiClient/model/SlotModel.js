import { DATE_FORMAT_ENUM } from '../../constants/enum';
import { dateFormatter } from '../../utility';
import {
    checkResourceNotNullOrEmpty,
    checkResourceNotNullElseZero,
} from '../../utility/ValidationUtil';

export const slotModel = (slotData, timezone) => {
    let payload = {};

    payload.id = checkResourceNotNullOrEmpty(slotData.id);
    payload.teacherId = checkResourceNotNullOrEmpty(slotData.teacherId);
    payload.startTime = checkResourceNotNullOrEmpty(slotData.startTime);
    payload.startMinute = checkResourceNotNullOrEmpty(slotData.startMinute);
    payload.endMinute = checkResourceNotNullOrEmpty(slotData.endMinute);
    payload.endTime = checkResourceNotNullOrEmpty(slotData.endTime);
    payload.dayOfWeek = checkResourceNotNullOrEmpty(slotData.dayOfWeek);
    payload.status = checkResourceNotNullOrEmpty(slotData.status);
    payload.isRecurring = checkResourceNotNullElseZero(slotData.recurring);
    payload.isActive = checkResourceNotNullElseZero(slotData.isActive);

    // To make rendering easy on component Layer
    payload.displayStartTime = dateFormatter(
        slotData.startTime,
        DATE_FORMAT_ENUM.TIME_FORMAT_AMPM,
        timezone,
    );
    payload.displayEndTime = dateFormatter(
        slotData.endTime,
        DATE_FORMAT_ENUM.TIME_FORMAT_AMPM,
        timezone,
    );
    if (slotData.teacher) {
        payload.teacher = teacher(slotData.teacher);
    }

    if (slotData.booking) {
        payload.booking = booking(slotData.booking);
    }
    return payload;
};

/**
 *  teacher model
 */
const teacher = (slotInfo) => {
    let payload = {};
    payload.id = checkResourceNotNullOrEmpty(slotInfo.id);
    payload.email = checkResourceNotNullOrEmpty(slotInfo.email);
    payload.name = checkResourceNotNullOrEmpty(slotInfo.name);
    return payload;
};

const booking = (slotInfo) => {
    let payload = {};
    payload.id = checkResourceNotNullOrEmpty(slotInfo.id);
    payload.status = checkResourceNotNullOrEmpty(slotInfo.status);
    payload.isTrial = checkResourceNotNullOrEmpty(slotInfo.isTrial);
    payload.courseType = checkResourceNotNullOrEmpty(
        slotInfo?.course_class?.course?.courseType,
        payload.courseClassType = checkResourceNotNullOrEmpty(slotInfo?.course_class?.courseClassType)
    );
    return payload;
};
