export const teacherModel = (teacherData) => {
    let payload = {};

    payload.id = teacherData?.id;
    payload.email = teacherData?.email;
    payload.name = teacherData.name;

    return payload;
};
