const endUrls = {
    authenticateCancelOTP: '/userDetail/authenticateToken',
    sendCancleOTP: '/users/sendVerificationCode',
    oneTimeScheduler: (studentId) =>
        `/bookings/students/${studentId}/oneTimeScheduler`,
    avaiableTeacherList: (studentId) =>
        `/students/${studentId}/getSubstituteTeacherForOneTimeScheduler`,
};

export default endUrls;
