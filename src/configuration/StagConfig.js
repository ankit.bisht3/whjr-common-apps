import { AppEnvironmentEnum } from "../constants/enum";

const StagConfig = {
  environment: AppEnvironmentEnum.STAGING,
  apis: {
    apiBaseUrl: 'https://stage-api.whjr.one/api/V1'
  }
};

export default StagConfig;
