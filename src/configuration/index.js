import ProdConfig from './ProdConfig';
import StagConfig from './StagConfig';
import UatConfig from './UatConfig';

//if both false then staging environment will run.
let isProductionEnvironment = false;
let isUatEnvironment = false;

if (__DEV__) {
    isProductionEnvironment = false;
    isUatEnvironment = false;
}

export default isProductionEnvironment
    ? ProdConfig
    : isUatEnvironment
        ? UatConfig
        : StagConfig;
