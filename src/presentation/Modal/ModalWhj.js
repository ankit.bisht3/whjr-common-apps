import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  StatusBar,
  ScrollView,
} from 'react-native';
import ImageWhj from '../Image';
import { ClassPopupsImages } from '../../common/Images';
import { isAndroid } from '../../utility';
import { hasNotch  } from 'react-native-device-info';

const ModalWhj = (props) => {
  const contentHeight = parseInt(props?.height, 10);
  const { isInfo } = props;
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Modal
        animationType={props.animationType}
        transparent={props.transparent}
        supportedOrientations={['portrait', 'landscape']}
        hardwareAccelerated={props.hardwareAccelerated}
        visible={props.visible}
        onRequestClose={() => {
          !isInfo ? props.setActionModalVisible(false) : {};
        }}
        style={styles.modalStyle}>
        <View
          style={styles.transparentContainer}
          onStartShouldSetResponder={() =>
            !isInfo ? props.setActionModalVisible(false) : {}
          }>
          {!isInfo ? (
            <TouchableOpacity
              onPress={() => {
                props.setActionModalVisible(false);
              }}
              style={styles.closeModalButtonStyle}>
              <ImageWhj
                imageData={{
                  source: ClassPopupsImages.closeDialog,
                  style: styles.closeIconStyle,
                }}
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={[
            styles.modalContainer,
            { alignItems: props.isLandscape ? 'center' : null },
          ]}>
          <View style={[styles.modalContent, props?.containerStyle]}>
            {props?.topHeader}
            <ScrollView
              showsVerticalScrollIndicator={props.isScrollIndicatorRequired}
              keyboardShouldPersistTaps={props.keyboardShouldPersistTaps}
              scrollEnabled={props.scrollEnabled}
              style={[props?.style, contentHeight && { height: contentHeight }]}>
              {props?.children}
            </ScrollView>
            {props?.bottomBar}
          </View>
        </View>
      </Modal>
    </>
  );
};

ModalWhj.propTypes = {
  height: PropTypes.number,
  transparent: PropTypes.bool,
  visible: PropTypes.bool,
  hardwareAccelerated: PropTypes.bool,
  animationType: PropTypes.string,
  keyboardShouldPersistTaps: PropTypes.string,
  scrollEnabled: PropTypes.bool,
  style: PropTypes.object,
  setActionModalVisible: PropTypes.func,
  isLandscape: PropTypes.bool,
};
ModalWhj.defaultProps = {
  style: {},
  transparent: true,
  visible: true,
  hardwareAccelerated: true,
  animationType: 'slide',
  keyboardShouldPersistTaps: 'never',
  scrollEnabled: false,
  isLandscape: false,
  setActionModalVisible: () => { },
};
export default ModalWhj;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  transparentContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'flex-end',
    flex: 1,
  },
  modalContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalStyle: {
    margin: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 16,
  },
  closeButtonStyle: {
    marginRight: 20,
    marginTop: 25,
  },
  closeModalButtonStyle: {
    marginRight: 20,
    marginTop: hasNotch() ? 60 : isAndroid() ? 15 : 25,
  },
  closeIconStyle: {
    width: 20,
    height: 20,
  },
});
