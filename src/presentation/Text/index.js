import React from 'react';
import { StyleSheet, Text } from 'react-native';

const TextWhj = ({
    title,
    type,
    style,
    numberOfLines,
    children,
    lineHeight,
    textAlign,
    lineArray,
    ellipsisReq,
    onPress,
    customColor,
    ellipsizeMode,
}) => (
    <Text
        onTextLayout={(event) => {
            if (lineArray) {
                lineArray(event.nativeEvent.lines);
            }
        }}
        numberOfLines={numberOfLines}
        allowFontScaling={false}
        {...(ellipsisReq
            ? { ellipsizeMode: ellipsizeMode || 'tail', numberOfLines: 1 }
            : {})}
        onPress={onPress}
        style={[
            styles.text,
            {
                fontSize: type?.fontSize,
                fontFamily: type?.fontFamily,
                color: customColor ?? type?.color,
                lineHeight: lineHeight,
                textAlign: textAlign,
            },
            style,
        ]}>
        {title}
        {children}
    </Text>
);

const styles = StyleSheet.create({
    text: {
        alignSelf: 'center',
    },
});

export default TextWhj;
