import React from 'react';
import { View, Image } from 'react-native';
import Config from '../../configuration';

const ImageWhj = (props) => {
    const {
        source = {},
        imageUrl = '',
        width = '100%',
        height = '100%',
        resizeMode = 'contain',
        style = {},
        imageData = {},
    } = props;
    const getImageSource = () => {
        if (imageData?.source) {
            return imageData?.source;
        } else if (Object.keys(source).length) {
            if (source?.uri) {
                return { uri: source?.uri, headers: { referer: Config.refererHeader } };
            } else {
                return source;
            }
        } else if (
            imageUrl &&
            imageUrl.includes('http') &&
            imageUrl.includes('//')
        ) {
            return { uri: imageUrl, headers: { referer: Config.refererHeader } };
        } else if (imageUrl) {
            return { imageUrl };
        } else if (source) {
            return source;
        }
    };
    const imageSrc = getImageSource();
    return imageSrc ? (
        <Image
            source={imageSrc}
            style={{
                width: width,
                height: height,
                ...style,
                ...imageData?.style,
            }}
            resizeMode={resizeMode}
        />
    ) : (
        <View
            style={{
                width: width,
                height: height,
                ...style,
                ...imageData?.style,
            }}
        />
    );
};

export default ImageWhj;
