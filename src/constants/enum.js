import { hasNotch } from 'react-native-device-info';
import { NAVIGATION_BAR_HEIGHT } from "../utility/SizeUtil";

export const AppEnvironmentEnum = {
    STAGING: 'staging',
    UAT: 'uat',
    PRODUCTION: 'Production',
};

export const SCREEN_TYPE_BOOSTER_CLASS = {
    REGULAR_CLASS: 'REGULAR_CLASS',
    PROJECT_BOOSTER: 'PROJECT_BOOSTER',
};

const isIOSDevice = () => {
    return Platform.OS === 'ios';
};

export const LOGIN_SCREEN_STATES = {
    LOGIN_WITH_PHONE: 'phone',
    LOGIN_WITH_EMAIL: 'email',
    LOGIN_WITH_OTP: 'otp',
    LOGIN_WITH_PASSWORD: 'password',
    FORGET_PASSWORD_PHONE: 'forget_mobile',
    FORGET_PASSWORD_OTP: 'forget_otp',
    FIND_ACCOUNT: 'find_account',
    RESEND_OTP: 'resend_otp',
    RESENT_OTP_TIMER: 50,
    KEYBOARD_HEIGHT: isIOSDevice() ? 250 : 240,
    KEYBOARD_SPACE: isIOSDevice()
        ? hasNotch()
            ? 16
            : 0
        : NAVIGATION_BAR_HEIGHT < 0
            ? 0
            : NAVIGATION_BAR_HEIGHT,
    KEYBOARD_TIME: 300,
};


export const OTP_TEXT_CONFIG = {
    OTP_VALIDITY: 90,
};

export const API_STATUS_CODES = {
    RESPONSE_200: 200,
    RESPONSE_400: 400,
    RESPONSE_502: 502,
    UNAUTHORIZED: 'unauthorized',
};

export const COURSE_TYPE_STATUS = {
    MATH: 'MATH',
    CODING: 'CODING',
    BOTH: 'BOTH',
    MUSIC: 'MUSIC',
};

export const DATE_FORMAT_ENUM = {
    TIME_FORMAT_AMPM: 'hh:mm A',
    THREE_CHAR_MONTH: 'MMM',
    TWO_CHAR_DATE: 'DD',
    THREE_CHAR_DATE: 'ddd',
    DATE_WITH_WEEK_DAY: 'Do MMMM dddd',
    DD_MMM_YYYY: 'DD MMM YYYY',
    DDD_MMM_YYYY: 'Do MMM YYYY',
    MM_DD_YYYY: 'MM-DD-YYYY',
    Do_MMM: 'Do MMM',
    MMM_Do: 'MMM Do',
    DAYS: 'days',
    SLOT_TIMING_WITH_DATE: 'Do MMM dddd h a',
    SLOT_TIMING_WITH_DAY_DATE_MONTH: 'Do MMM, dddd, hh:mm a',
    SLOT_TIMING_WITH_DAY_DATE_MONTH_WITHOUT_ZERO_PADDING: 'Do MMM, dddd, h:mm A',
    SLOT_TIMING_WITH_DAY_DATE_MONTH_INTERVAL: 'Do MMM, dddd, ',
    FORMATE_1: 'dddd, DD MMM YYYY',
    DD_MM_YYYY: 'DD-MM-YYYY',
    YYYY_MM_DD: 'YYYY_MM_DD',
    HH_MM: 'HH:mm',
    SECONDS: 'seconds',
    HOURS: 'hours',
    YYYY_MM_DD_HH_MM_SS: 'YYYY-MM-DD HH:mm:ss',
    DAY_DATE_MONTH: 'ddd DD MMM',
    DAY_D0_MMMM_TIME: 'ddd Do MMMM, hh:mm A',
    D0_MMMM_TIME: 'Do MMMM, hh:mm a',
    D0_MMMM_YYYY_TIME: 'Do MMM YYYY, hh:mm a',
    DAY_MONTH_DATE: 'Do MMM, ddd',
    DAY_MONTH_YEAR: 'Do MMM, YYYY',
    D_MM_YYYY: 'Do MMM YYYY',
    HH_MM_DATE: 'hh:mm A Do MMM',
};

export const CLASS_TYPE = {
    UPCOMING: 'upcoming',
    ALL_UPCOMING: 'all_upcoming',
    COMPLETED: 'completed',
    NOT_COMPLETED: 'not_completed',
    NOTCOMPLETED: 'notcompleted',
    CONFIRMED: 'confirmed',
    STARTED: 'started',
    CANCELLED: 'cancelled',
    EXPIRED: 'expired',
    YTS: 'YTS',
    UPCOMING_TRIAL_CLASSES: 'upcoming_trial_classes',
    ONE_TO_MANY: 'ONE_TO_MANY',
    ONE_TO_ONE: 'ONE_TO_ONE',
    NONE: 'none',
};

export const CLASS_MODAL_TYPES = {
    UPCOMING: 'upcoming',
    COMPLETED: 'completed',
    NOT_COMPLETED: 'not_completed',
    NO_MODAL: 'modal',
    IS_VISIBLE: 'isVisible',
    SLOT_CONFIRMED: 'slot_confirmed',
    SLOT_CANCELLED: 'slot_cancelled',
    CANCEL_SLOT_CLASS: 'cancel_slot_class',
    SCHEDULE_CLASS: 'schedule_class',
    TEACHER_SUBSTITUTE: 'teacher_substitue',
    COVID_19_THRESHOLD: 'COVID_19_THRESHOLD',
    NO_CREDIT_LEFT: 'NO_CREDIT_LEFT',
    BOOSTER_CONFIRMED: 'BOOSTER_CONFIRMED',
    TERMS_AND_CONDITIONS: 'TERMS_AND_CONDITIONS',
    DAY_LIMIT_REACHED: 'day_limit_reached'
}

export const WEB_URLS = {
    renew: (url, token) =>
        `${url}s/renewal?jwt_token=${token}&hide_mobile_navigation`,
};

export const LIMIT_REACHED = {
    DAY: 'Day',
    WEEK: 'Week',
};

export const CLASS_BOOKED_VIA_THIS_DEVICE = {
    CLASS_BOOKED_VIA_THIS_DEVICE: 3,
};

export const PROJECT_BOOSTER_EVENT = {
    NORMAL: 'normal',
    PROJECT_BOOSTER: 'PROJECT_BOOSTER',
};

export const SCHEDULE_WEEK_TITLE_ENUM = {
    WEEK_1: 'Week 1',
    WEEK_2: 'Week 2',
    WEEK_3: 'Week 3',
    WEEK_4: 'Week 4',
    WEEK_5: 'Week 5',
    WEEK_6: 'Week 6',
};

export const WORKING_HOUR_RANGE = {
    IN: {
        startMinute: 480,
        endMinute: 1260,
    },
    default: {
        startMinute: 510,
        endMinute: 1290,
    },
};

export const USER_TYPE = {
    TRIAL: 'trial',
    PAID: 'paid',
};

export const POPUP_STATE = {
    CLASS_DETAIL: 'class_details',
    CANCEL_CLASS: 'cancel_class',
    CLASS_CANCEL_NO_DETAIL: 'class_cancel_no_detail',
};
