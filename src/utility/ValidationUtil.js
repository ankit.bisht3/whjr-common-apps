import { trimString } from ".";

/**
 * Function to check if a particular resource is null else return 0
 * @param {*} val
 */
export const checkResourceNotNullOrEmpty = (val) => {
    if (typeof val !== 'undefined' && val !== null) {
        return val;
    } else {
        return '';
    }
};

export const emptyResourceWillNotOverwrite = (newValue, oldValue) => {
    if (newValue === null || newValue === '') {
        return oldValue;
    } else {
        return newValue;
    }
};

/**
 * Function to check if a particular resource is null else return 0
 * @param {*} val
 */
export const checkResourceNotNullElseZero = (val) => {
    return val ? val : 0;
};

export const checkFeedbackText = (value) => {
    let nevValue = trimString(value);
    return nevValue.length >= 5 && nevValue.length <= 500;
};
