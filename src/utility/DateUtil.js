import moment from 'moment-timezone';
import { DATE_FORMAT_ENUM } from '../constants/enum';

let timeZone = moment.tz.guess();

export const getTZDate = (date) => {
    return moment(date).tz(timeZone);
};

export const format = (date) =>
    getTZDate(date).format(DATE_FORMAT_ENUM.YYYY_MM_DD);

export const momentLocale = () => {
    moment.updateLocale('en', {
        week: {
            dow: 1,
        },
    });
    return moment;
};

/**
 * Function to check if there is credit loss for canceling a class or not
 */
 export const checkClassCancelTime = (slotStartTime, userTimezone) => {
    if (slotStartTime) {
      const startTime = moment(slotStartTime);
    //   .tz(userTimezone);
      let currentTime = moment();
      let timeDiff = moment.duration(startTime.diff(currentTime));
      return timeDiff.as('hours') <= 24;
    }
    return false;
  };