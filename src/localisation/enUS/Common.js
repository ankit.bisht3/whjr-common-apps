import { IS_BYJUS_APP } from '../../configuration/byjus';
const appName = IS_BYJUS_APP ? "BYJU'S FutureSchool" : 'WhiteHat Jr';

const Common = {
  recommendClassesText: (limit) => `We recommend ${limit} classes every week.`,
  continue: 'Continue',
  selectTeacher: 'Select Your Teacher',
  primaryTeacher: 'Your Teacher',
  substituteText: 'Substitute',
  noSlotAvailable: 'No Slots Available',
  renewPlanBtnText: 'RENEW YOUR PLAN',
  courseExpiredtext: 'Your Course Has Expired',
  renewPlanDescriptionText:
    'Continue to book your class, Please renew your plan.',
  covidPopupHeader: (limit) => `Covid-19 Lockdown ${limit} class Maximum!`,
  covidPopupDescription: (limit) =>
    `Our teacher slots are running 100% booked out due to unprecedented Lockdown demand. We request your kind assistance in restricting classes to ${limit}x/week to allow other students to benefit from the learning.`,
  covidPopupFooter:
    'We’re here for you! Please reach out to us for more projects. Our team is working 24x7 to meet your learning needs.',
  covidPopupBtnText: 'Yes, I Understand',  
  back: 'Back',
  youHaveSentOTP: 'We have sent you an OTP on ',
  youHaveSentOTPAnd: 'and ',
  otpInvalidText: 'Invalid OTP',
  oneCredit: '1 credit',
  creditConsumedMsg: 'will be consumed for this class.',
  confirmSlotButtontext: 'CONFIRM SLOT',
  confirm: 'Confirm',
  selectClassType: 'Select Class Type',
  projectBoosterClassDesc:
    'A project booster is a class where the teacher helps you complete your pending projects.',
  worksheetBoosterClassDesc:
    'A Worksheet booster is a class where the teacher helps you complete your pending Worksheets.',
  notes: 'Note:',
  projectBoosterClass: 'Project Booster Class',
  worksheetBoosterClass: 'Worksheet Booster Class',
  regularClass: 'Regular Class',
  selectClassType: 'Select Class Type',  
  scheduleNowClass: 'Schedule Now',
  yayClassScheduled: 'Yay! Class Scheduled.',
  classConfirmed: (slotTime) =>
    `Your class has been confirmed for ${slotTime}.`,
  slotCancelled: 'Class Cancelled',
  slotCancelledConfirmed: (slotTime) =>
    `Your class for ${slotTime}, has been cancelled.`,
  boosterClassConfirmation: 'Yay! Project Booster Class Scheduled.',
  worksheetClassConfirmation: 'Yay! Worksheet Booster Class Scheduled.',
  backPopup: 'Back',
  dayLimitHeading: 'Unable to schedule',
  dayLimitDescription:
    'For optimum learning we recommend a max of 1 class per course per day. Please cancel your scheduled class first to schedule a different time slot.',
  yesCancelMyClass: 'Yes, Cancel My Class',
  cancelClassText: 'Cancel Class',
  okPopup: 'OK',
  iUnderstand: 'I understand I will',
  loseOneCredit: 'lose 1 credit',
  cancellingWithLessThanTwentyFourHours:
    'as I am cancelling with less than 24 hours left for the slot.',
  ptmClass: {
      bannerNote:
        'Please note that your next class will be a meeting with the teacher',
      inSpotlight: 'Parent-Teacher Conference',
      meetingClass: 'Please make sure you join the class.',
      afterClassBanner: 'Thanks for meeting up with the teacher!',
      scheduleNextClass: 'Please schedule the next class at your convenient time',
  },
  areYouSureWantToCancelClass: 'Are you sure, you want to cancel this class?',
  project_Booster: 'PROJECT_BOOSTER',
  booster_Class: 'Booster Class',
  cancelClassPopup: 'Cancel Class',
  youUnderstand: 'You will',
  youCancellingWithLessThanTwentyFourHours:
  'as you are cancelling with less than 24 hours left for the slot.',
};

export default Common;
