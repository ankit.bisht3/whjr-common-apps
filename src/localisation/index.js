import { IS_BYJUS_APP } from '../configuration/byjus';
import byzusEnglish from '../localisation/enUS/byjus';
import Common from './enUS/Common';
import whitehat from './whitehat';

const commonStrings = IS_BYJUS_APP
  ? byzusEnglish
  : { ...byzusEnglish, ...whitehat };

// const getLocalisation = () => {
//   return new LocalizedStrings({
//     default: { ...Common, ...commonStrings },
//   });

// };

const getLocalisation = () => {
  return {
    ...Common, ...commonStrings
  };

};

export default getLocalisation();
