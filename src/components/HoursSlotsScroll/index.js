import React, {Component} from 'react';
import {FlatList, TouchableOpacity, View} from 'react-native';
import { CLASS_TYPE, SCREEN_TYPE_BOOSTER_CLASS, COURSE_TYPE_STATUS, USER_TYPE} from '../../constants/enum';
import TextWhj from '../../presentation/Text';
import NoSlotView from '../noSlotView';
import TextTypes from '../../presentation/Text/TextTypes';
import styles from './styles';
import { camelCase, isCourseTypeMath, isCourseTypeCoding } from '../../utility';
import Common from '../../localisation/enUS/Common';

export default class HoursSlotsScroll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenProjectBooster: false,
      screenMathBooster: false,
      screenProjectRegular: false,
      screenMathRegular: false,
      isMathBooster: false,
      isProjectBooster: false,
    };
  }

  getTitle(slot) {
    return slot?.booking?.id &&
      slot?.booking?.courseClassType ===
        SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER
      ? Common.booster
      : slot?.booking?.courseType;
  }

  disableSlotClick(slot) {
    return slot.booking &&
      (slot.booking.status === CLASS_TYPE.COMPLETED ||
        slot?.booking?.courseClassType?.toLowerCase() === USER_TYPE.TRIAL)
      ? true
      : false;
  }

  renderHourItem = (slotsIndex, {item: slot, index}) => {
    const {onSlotPress, weekDetails, screenType, selectedCourseType} =
      this.props;

    const slots = weekDetails.days[slotsIndex].slots;

    this.state.screenProjectBooster =
      screenType === SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER &&
      selectedCourseType === COURSE_TYPE_STATUS.CODING;

    this.state.screenMathBooster =
      screenType === SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER &&
      selectedCourseType === COURSE_TYPE_STATUS.MATH;

    this.state.screenProjectRegular =
      screenType === SCREEN_TYPE_BOOSTER_CLASS.REGULAR_CLASS &&
      selectedCourseType === COURSE_TYPE_STATUS.CODING;

    this.state.screenMathRegular =
      screenType === SCREEN_TYPE_BOOSTER_CLASS.REGULAR_CLASS &&
      selectedCourseType === COURSE_TYPE_STATUS.MATH;

    this.state.isProjectBooster =
      slot?.booking?.courseClassType ===
        SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER &&
      isCourseTypeCoding(slot?.booking?.courseType);

    this.state.isMathBooster =
      slot?.booking?.courseClassType ===
        SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER &&
      isCourseTypeMath(slot?.booking?.courseType);

    const highlightViewStyle = () => {
      if (
        slot?.booking &&
        (slot?.booking?.status === CLASS_TYPE.COMPLETED ||
          (slot?.booking?.courseType !== selectedCourseType &&
            slot?.booking?.courseClassType?.toLowerCase() === USER_TYPE.TRIAL))
      ) {
        return styles.ClassCompletedStyle;
      } else {
        if (slot?.booking && slot?.booking?.id) {
          if (this.state.screenProjectRegular) {
            if (
              isCourseTypeMath(slot?.booking?.courseType) ||
              this.state.isMathBooster
            ) {
              return styles.slotColorStyle;
            }
            if (this.state.isProjectBooster) {
              return styles.boosterButtonStyle;
            }
            return styles.selectedSlotButtonStyles;
          } else if (this.state.screenMathRegular) {
            if (this.state.isMathBooster) {
              return styles.boosterButtonStyle;
            }
            if (
              this.state.isProjectBooster ||
              isCourseTypeCoding(slot?.booking?.courseType)
            ) {
              return styles.slotColorStyle;
            }
            return styles.selectedMathSlotButtonStyles;
          } else if (this.state.screenProjectBooster) {
            if (this.state.isProjectBooster) {
              return styles.boosterButtonStyle;
            }
            if (
              isCourseTypeCoding(slot?.booking?.courseType) ||
              isCourseTypeMath(slot?.booking?.courseType) ||
              this.state.isMathBooster
            ) {
              return styles.slotColorStyle;
            }
          } else if (this.state.screenMathBooster) {
            if (this.state.isMathBooster) {
              return styles.boosterButtonStyle;
            }
            if (
              isCourseTypeCoding(slot?.booking?.courseType) ||
              isCourseTypeMath(slot?.booking?.courseType) ||
              this.state.isProjectBooster
            ) {
              return styles.slotColorStyle;
            }
          } else {
            return styles.selectedSlotButtonStyles;
          }
        }
      }
    };

    return (
      <TouchableOpacity
        onPress={() => {
          onSlotPress(slot, weekDetails, slots);
        }}
        disabled={this.disableSlotClick(slot)}
        style={[styles.slotStyle]}>
        <View style={[styles.commonSlotStyle, highlightViewStyle()]}>
          <TextWhj
            title={slot.displayStartTime}
            type={TextTypes.bodyFourDemiText}
            style={highlightViewStyle()}
          />
          {slot?.booking?.id && (
            <TextWhj
              title={camelCase(
                !slot?.booking?.courseType
                  ? selectedCourseType
                  : this.getTitle(slot),
              )}
              type={TextTypes.bodyFourDemiText}
              style={highlightViewStyle()}
            />
          )}
        </View>
      </TouchableOpacity>
    );
  };

  renderItem = ({item, index}) => {
    const isSlotsAvailable = item?.slots && item?.slots?.length > 0;

    if (isSlotsAvailable) {
      return (
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={item.slots}
          renderItem={(data) => this.renderHourItem(index, data)}
          contentContainerStyle={styles.contentContainerStyle}
          keyExtractor={(_, i) => `slot-${i}`}
        />
      );
    }

    return <NoSlotView style={{marginLeft: 4}} />;
  };

  render() {
    const {weekDetails} = this.props;

    return (
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={weekDetails.days}
        renderItem={this.renderItem}
        keyExtractor={(_, i) => `slot-row-${i}`}
      />
    );
  }
}
