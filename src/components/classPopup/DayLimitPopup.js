import React from 'react';
import {View} from 'react-native';
import Localisation from '../../localisation';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';

const DayLimitPopup = (props) => {
  return (
    <ModalWhj
      scrollEnabled={true}
      setActionModalVisible={(value) => props.setActionModalVisible(value)}
      bottomBar={
        <View style={Styles.renewButtonContainer}>
          <ButtonWhj
            onPress={(value) => props.setActionModalVisible(value)}
            title={Localisation.backPopup}
            type={ButtonTypes.purpleMediumButton}
            width={150}
          />
        </View>
      }>
      <View style={Styles.renewPopupContainer}>
        <TextWhj
          style={Styles.renewHeader}
          title={Localisation.dayLimitHeading}
          type={TextTypes.bodyOneDemiText}
        />
        <TextWhj
          style={Styles.dayLimitDescriptionStyle}
          title={Localisation.dayLimitDescription}
          type={TextTypes.bodyThreeBookText}
        />
      </View>
    </ModalWhj>
  );
};

export default DayLimitPopup;
