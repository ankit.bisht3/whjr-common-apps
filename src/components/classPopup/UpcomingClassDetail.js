import React, {Component} from 'react';
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
// import {BoxShadow} from 'react-native-shadow';
import Localisation from '../../localisation';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';
import OTPTextView from '../otp/OTPTextView';
import BookedClassDetails from './common/BookedClassDetails';
import { BoosterImages, ptmClassIcon, ClassPopupsImages, ClassScreenImages } from '../../common/Images';
// import {updateReloadStatus} from '../../../redux/actions/GlobalAction';
// import {getLoader} from '../../../redux/actions/LoaderAction';
import {
  camelCase,
  trimString,
  isCourseTypeMath,
} from '../../utility';
// import {
//   cancelUpcomingStudentClass,
//   validateToken, *******************
//   sendVerificationCode, *******************
//   getClass,
//   getCourseList,
// } from '../../../redux/actions/ClassesAction';
// import {recordEvent} from '../../../manager/analytics';
// import {EVENT_TYPE} from '../../../manager/analytics/AnalyticsEventType';
import { checkClassCancelTime } from '../../utility/DateUtil';
import { POPUP_STATE, OTP_TEXT_CONFIG, API_STATUS_CODES, COURSE_TYPE_STATUS} from '../../constants/enum';
import { cardWithShadow } from '../../common/GlobalShadowStyle';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../utility/SizeUtil';
import { isAndroid, isIOS } from '../../utility';
import Colors from '../../common/Colors';

class UpcomingClassDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClassCancel: this.props.showClassDescription
        ? this.props.showClassDescription
        : false,
      isConfirmed: false,
      isCheckboxConfirmed: false,
      otp: '',
      popupState: this.props.initialPopupState,
      validatingOTP: false,
      isCreditLoss: false,
      resendFlag: false,
      resendFlagEnable: false,
      popupHeight: isAndroid ? SCREEN_HEIGHT * 0.35 : 0,
    };
    this._keyboardWillShow = this._keyboardWillShow.bind(this);
    this._keyboardWillHide = this._keyboardWillHide.bind(this);
  }

  componentDidMount() {
    console.log('UPcomingClasDetail called data props : ', this.props.data);
    let isCreditLoss = checkClassCancelTime(
      this.props?.data?.startTime,
      this.props?.timeZone,
    );
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({isCreditLoss: isCreditLoss});
    if (isAndroid()) {
      this.keyboardWillShowSub = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardWillHide,
      );
    }
  }

  componentWillUnmount() {
    // if (isAndroid()) {
    //   this.keyboardWillShowSub.remove();
    //   this.keyboardWillHideSub.remove();
    // }
  }

  _keyboardWillShow(event) {
    this.setState({popupHeight: SCREEN_HEIGHT * 0.4});
  }

  _keyboardWillHide(event) {
    this.setState({popupHeight: SCREEN_HEIGHT * 0.65});
  }

  resendOTPFlag = (status) => {
    this.setState({resendFlag: status});
  };

  resetResendFlag = (status) => {
    this.setState({resendFlagEnable: status});
  };

  async resendOTP() {
    const response = await this.props.sendVerificationCode();
    if (response.status === API_STATUS_CODES.RESPONSE_200) {
      this.setState({
        resendFlag: true,
        resendFlagEnable: false,
        otp: '',
        validatingOTP: false,
      });
    }
  }

  handleCancelMyClass = () => {
    // recordEvent(EVENT_TYPE.classes.PAIDCLASS_CANCELLED_SUCCESS, {
    //   count_credit_lost: this.state.isCreditLoss,
    // });
    if (this.state.isCreditLoss) {
      if (this.state.isCheckboxConfirmed && !this.props.isOtpOnCancellation) {
        this.props.cancelBookedSlot();
      } else if (
        this.state.isCheckboxConfirmed &&
        this.props.isOtpOnCancellation
      ) {
        this.setState({popupState: POPUP_STATE.CANCEL_CLASS});
        this.props.sendVerificationCode();
      } else {
        this.setState({isConfirmed: true});
      }
    } else {
      if (!this.props.isOtpOnCancellation) {
        this.props.cancelBookedSlot();
      } else if (this.props.isOtpOnCancellation) {
        this.setState({popupState: POPUP_STATE.CANCEL_CLASS});
        this.props.sendVerificationCode();
      } else {
        this.setState({isConfirmed: true});
      }
    }
  };

  cancelUpcomingClass = () => {
    const {otp} = this.state;
    this.props.validateToken(otp).then((res) => {
      if (res?.data?.success) {
        this.props.cancelBookedSlot();
      } else {
        this.setState({validatingOTP: true});
      }
    });
  };

  handleTextChange = (field) => (text) => {
    const {validatingOTP} = this.state;
    const newState = {};
    newState[field] = trimString(text);
    if (field === 'otp' && validatingOTP) {
      newState.validatingOTP = false;
    }
    this.setState(newState);
  };

  renderBottomBar() {
    const {isClassCancel, isConfirmed, isCheckboxConfirmed, popupState, otp} =
      this.state;
    const isOTPValid = otp.length === 4;
    return (
      <View style={Styles.innerBoxSummaryBottomContainer}>
        {isClassCancel && (popupState === POPUP_STATE.CLASS_DETAIL || popupState === POPUP_STATE.CLASS_CANCEL_NO_DETAIL) && (
          <>
            <ButtonWhj
              type={ButtonTypes.mediumPlainButton}
              onPress={() => this.props.setActionModalVisible(false)}
              title={Localisation.backPopup}
              animationDisabled
              isDebounce={false}
              textColor={popupState === POPUP_STATE.CLASS_CANCEL_NO_DETAIL ? Colors.orangeLight : Colors.greyNew}
            />
            <ButtonWhj
              onPress={() => this.handleCancelMyClass()}
              title={Localisation.yesCancelMyClass}
              style={Styles.rightMostButtonStyle}
              type={
                isConfirmed && !isCheckboxConfirmed
                  ? ButtonTypes.greyMediumButton
                  : ButtonTypes.redBigButton
              }
              animationDisabled
              isDebounce={false}
            />
          </>
        )}
        {popupState === POPUP_STATE.CANCEL_CLASS && (
          <>
            <ButtonWhj
              type={ButtonTypes.plainButton}
              onPress={() => this.props.setActionModalVisible(false)}
              title={Localisation.backPopup}
              animationDisabled
              isDebounce={false}
            />
            <ButtonWhj
              onPress={() => this.cancelUpcomingClass()}
              title={Localisation.yesCancelMyClass}
              style={Styles.rightMostButtonStyle}
              type={
                isOTPValid
                  ? ButtonTypes.purpleBigButton
                  : ButtonTypes.greyMediumButton
              }
              animationDisabled
            />
          </>
        )}
        {!isClassCancel && (
          <>
            <ButtonWhj
              onPress={() => this.setState({isClassCancel: true})}
              type={ButtonTypes.mediumPlainButton}
              title={Localisation.cancelClassText}
              animationDisabled
              textColor={Colors.greyNew}
              isDebounce={false}
            />
            <ButtonWhj
              onPress={() => this.props.setActionModalVisible(false)}
              title={Localisation.okPopup}
              style={Styles.cancelClassrightMostButtonStyle}
              type={ButtonTypes.purpleMediumButton}
              animationDisabled
            />
          </>
        )}
      </View>
    );
  }

  renderCreditLossCheckbox = () => {
    const {data, mobile, email} = this.props;
    const {
      isClassCancel,
      isConfirmed,
      isCheckboxConfirmed,
      isCreditLoss,
    } = this.state;
    return (
      <>
        {isCreditLoss && isClassCancel && isConfirmed && (
          <View style={Styles.confirmationBoxStyle}>
            <TouchableOpacity
              onPress={() =>
                this.setState({isCheckboxConfirmed: true})
              }>
              <Image
                style={Styles.uncheckedBoxStyle}
                source={
                  isCheckboxConfirmed
                    ? isCourseTypeMath(this.props.courseType)
                      ? ClassPopupsImages.blueCheckedCheckbox
                      : ClassPopupsImages.checkedCheckbox
                    : ClassPopupsImages.uncheckedCheckbox
                }
              />
            </TouchableOpacity>
            <TextWhj
              title={Localisation.iUnderstand}
              type={TextTypes.bodyThreeDemiText}
              style={[
                Styles.commonColorBlack,
                Styles.losseCreditStyle,
              ]}>
              {' '}
              <TextWhj
                title={Localisation.loseOneCredit}
                type={TextTypes.bodyThreeDemiText}
                style={Styles.loseCreditTextStyle}>
                {' '}
              </TextWhj>
              <TextWhj
                title={
                  Localisation.cancellingWithLessThanTwentyFourHours
                }
                type={TextTypes.bodyThreeDemiText}
                style={[
                  Styles.commonColorBlack,
                  Styles.losseCreditStyle,
                ]}
              />
            </TextWhj>
          </View>
        )}
      </>
    )
  }

  renderClassDetails = () => {
    const {data} = this.props;
    const {isClassCancel} = this.state;
    return (
      <>
        {data?.classDescription && !isClassCancel && (
          <>
            <TextWhj
              title={data?.classDescription ? data.classDescription : ''}
              type={TextTypes.bodyTwoBookText}
              style={[
                Styles.commonColorBlack,
                Styles.upcomingClassDefinitionStyle,
              ]}
            />
            <View style={Styles.greyLineStyle} />
          </>
        )}
        <BookedClassDetails
          classData={data}
          isTeacherEmail={this.state.isClassCancel}
          isSubstitute={this.props?.defaultTeacherId !== data?.teacherId}
        />
      </>
    )
  }

  render() {
    const {data, mobile, email} = this.props;
    const {
      isClassCancel,
      isConfirmed,
      isCheckboxConfirmed,
      popupState,
      validatingOTP,
      isCreditLoss,
      resendFlag,
      popupHeight,
    } = this.state;
    return (
      <ModalWhj
        style={Styles.popupStyle}
        scrollEnabled={true}
        height={
          popupState === POPUP_STATE.CANCEL_CLASS
            ? isAndroid()
              ? popupHeight
              : SCREEN_HEIGHT * 0.7
            : 0
        }
        keyboardShouldPersistTaps="always"
        containerStyle={Styles.modelContainerStyle}
        setActionModalVisible={(value) =>
          this.props.setActionModalVisible(value)
        }
        topHeader={
          <>
            {/* <BoxShadow
              setting={cardWithShadow(
                SCREEN_WIDTH,
                68,
                '#000000',
                0.04,
                0,
                2,
                20,
              )}> */}
              <View style={[Styles.classSummaryHeaderOuterStyleContainer,{borderTopRightRadius:  data?.isPtmTag && data?.isPtmClass ? 0 : 20, borderTopLeftRadius: data?.isPtmTag && data?.isPtmClass ? 0 : 20}]}>
                {data?.isPtmTag && data?.isPtmClass ? (
                  <View style={Styles.spotlightContainer}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        style={Styles.spotlightIcon}
                        source={ptmClassIcon.artwork}
                        resizeMode={'contain'}
                      />
                      <View style={Styles.spotlightTextContainer}>
                        <TextWhj
                          title={Localisation.ptmClass.inSpotlight}
                          type={TextTypes.bodyOneDemiText}
                          style={{alignSelf: 'flex-start'}}
                        />
                        <TextWhj
                          title={Localisation.ptmClass.meetingClass}
                          type={TextTypes.bodyFourBookText}
                          style={{color: Colors.whiteWhj}}
                        />
                      </View>
                    </View>
                  </View>
                ) : null}

                <View
                  style={[
                    Styles.classSummaryStyleContainer,
                    Styles.marginHorizontal16,
                    {top: data?.isPtmTag && data?.isPtmClass ? -25 : 0 },
                  ]}>
                  {popupState === POPUP_STATE.CLASS_CANCEL_NO_DETAIL ? (
                    <TextWhj
                      title={Localisation.areYouSureWantToCancelClass}
                      type={TextTypes.bodyOneBookText}
                      style={Styles.commonColorBlack}
                    />
                    ) : (
                    <>
                      {data?.isBooster === Localisation.project_Booster ? (
                        <>
                          <Image
                            style={Styles.BoosterIcon}
                            source={BoosterImages.boosterIcon}
                            resizeMode={'contain'}
                          />
                          <TextWhj
                            title={data?.courseType === COURSE_TYPE_STATUS.MATH ? Localisation.worksheetBoosterClass : Localisation.projectBoosterClass}
                            type={TextTypes.bodyOneDemiText}
                            style={Styles.commonColorBlack}
                          />
                        </>
                      ) : (
                        <>
                          {data?.className === Localisation.booster_Class ? (
                            <Image
                              style={Styles.BoosterIcon}
                              source={BoosterImages.boosterIcon}
                              resizeMode={'contain'}
                            />
                          ) : (
                            <ImageBackground
                              style={Styles.imageBackgroud}
                              source={ClassScreenImages.blueRectangleIcon}>
                              <Image
                                style={Styles.techIconStyle}
                                source={ClassScreenImages.techIcon}
                                resizeMode={'contain'}
                              />
                            </ImageBackground>
                          )}
                          <View style={Styles.popUpHeading}>
                            {data?.classNumber ? (
                              <TextWhj
                                title={`${
                                  data?.classNumber.split('-')[1]
                                }-${camelCase(data?.className)}`}
                                type={TextTypes.bodyTwoDemiText}
                                style={Styles.commonColorBlack}
                              />
                            ) : (
                              <TextWhj
                                title={Localisation.cancelClassPopup}
                                type={TextTypes.bodyOneDemiText}
                                style={Styles.commonColorBlack}
                              />
                            )}
                          </View>
                        </>
                      )}
                    </>)
                  }
                </View>
              </View>
            {/* </BoxShadow> */}
          </>
        }
        bottomBar={
          isIOS() && (
            // <BoxShadow
            //   setting={cardWithShadow(
            //     SCREEN_WIDTH,
            //     80,
            //     '#000000',
            //     0.04,
            //     0,
            //     -2,
            //     20,
            //   )}>
              <View style={Styles.iosSummaryBottomContainer}>
                {this.renderBottomBar()}
              </View>
            // </BoxShadow>
          )
        }>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          horizontal={false}
          automaticallyAdjustContentInsets={false}
          contentContainerStyle={Styles.scrollContainerStyle}
          enableOnAndroid
          style={Styles.scrollStyle}>
          <View style={[Styles.marginHorizontal16, popupState !== POPUP_STATE.CLASS_CANCEL_NO_DETAIL && Styles.marginTop16]}>
            {popupState === POPUP_STATE.CLASS_CANCEL_NO_DETAIL && this.renderCreditLossCheckbox()}
            {popupState === POPUP_STATE.CLASS_DETAIL && (
              <>
                {this.renderClassDetails()}
                {isClassCancel && (
                  <View style={Styles.cancleClassText}>
                    <TextWhj
                      title={Localisation.areYouSureWantToCancelClass}
                      type={TextTypes.bodyOneMediumBlackText}
                    />
                    {this.renderCreditLossCheckbox()}
                  </View>
                )}
              </>
            )}
            {popupState === POPUP_STATE.CANCEL_CLASS && (
              <>
              {this.renderClassDetails()}
                <View style={Styles.otpContainer}>
                  <TextWhj
                    title={Localisation.youHaveSentOTP}
                    style={Styles.otpDetailsContainer}
                    type={TextTypes.bodyTwoBookText}>
                    {mobile} {Localisation.youHaveSentOTPAnd} {email}
                    {'.'}
                  </TextWhj>
                  <View style={Styles.cancleClassText}>
                    <OTPTextView
                      ref={(e) => (this.otpInput = e)}
                      autoFocusOnLoad
                      resendFlag={resendFlag}
                      validatingOTP={validatingOTP}
                      resendOTPFlag={(value) => this.resendOTPFlag(value)}
                      resendCancelClassOTP={() => this.resendOTP()}
                      isClassCancel
                      otpTime={OTP_TEXT_CONFIG.OTP_VALIDITY}
                      containerStyle={Styles.cancleClassOTPBottomMargin(
                        validatingOTP ? 0 : 8,
                      )}
                      handleTextChange={this.handleTextChange('otp')}
                    />
                    {validatingOTP && (
                      <TextWhj
                        title={Localisation.otpInvalidText}
                        style={Styles.cancelClassInvalidStyle}
                        type={TextTypes.bodyTwoDemiText}
                      />
                    )}
                    {isCreditLoss && (
                      <TextWhj
                        title={Localisation.youUnderstand}
                        type={TextTypes.bodyTwoMediumBlackText}
                        style={Styles.yourWillLosseCreditStyle}>
                        {' '}
                        <TextWhj
                          title={Localisation.loseOneCredit}
                          type={TextTypes.bodyTwoMediumText}
                          style={Styles.lose1CreditTextStyle}>
                          {' '}
                        </TextWhj>
                        <TextWhj
                          title={
                            Localisation.youCancellingWithLessThanTwentyFourHours
                          }
                          type={TextTypes.bodyTwoMediumBlackText}
                          style={Styles.losseCreditStyle}
                        />
                      </TextWhj>
                    )}
                  </View>
                </View>
              </>
            )}
          </View>
          {isAndroid() && (
            <View style={Styles.androidSummaryBottomContainer}>
              {this.renderBottomBar()}
            </View>
          )}
        </KeyboardAwareScrollView>
      </ModalWhj>
    );
  }
}

UpcomingClassDetail.propTypes = {};

UpcomingClassDetail.defaultProps = {
  initialPopupState: POPUP_STATE.CLASS_DETAIL,
}

export default UpcomingClassDetail;

// const mapStateToProps = ({classReducer, userInfo, globalReducer}) => {
//   const courseType = userInfo?.selectedCourseType;
//   const teacherId = userInfo?.userInfo?.teacherId;
//   const mathTeacherId = userInfo?.userInfo?.mathTeacherId;
//   const selectedCourseType = userInfo?.selectedCourseType;
//   return {
//     // allClassData: classReducer?.allClassData,
//     timeZone: userInfo?.userInfo?.timezone,
//     isOtpOnCancellation: userInfo?.userInfo?.isOtpOnCancellation,
//     mobile: userInfo?.userInfo?.mobile,
//     email: userInfo?.userInfo?.email,
//     courseType: selectedCourseType,
//     defaultTeacherId: isCourseTypeCoding(selectedCourseType)
//       ? teacherId
//       : mathTeacherId,
//   };
// };

// export default connect(mapStateToProps, {
//   getCourseList,
//   getClass,
//   cancelUpcomingStudentClass,
//   validateToken,
//   sendVerificationCode,
//   updateReloadStatus,
//   getLoader,
// })(UpcomingClassDetail);
