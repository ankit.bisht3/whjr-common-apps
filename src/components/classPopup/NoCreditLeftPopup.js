import React from 'react';
import {View} from 'react-native';
import localisation from '../../localisation';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';

const NoCreditLeftPopup = (props) => {
  return (
    <ModalWhj
      scrollEnabled={true}
      setActionModalVisible={(value) => props.setActionModalVisible(value)}
      bottomBar={
        <View style={Styles.renewButtonContainer}>
          <ButtonWhj
            onPress={props.onRenewClick}
            title={localisation.renewPlanBtnText}
            type={ButtonTypes.orangeBigButton}
          />
        </View>
      }>
      <View style={Styles.renewPopupContainer}>
        <TextWhj
          style={Styles.renewHeader}
          title={localisation.courseExpiredtext}
          type={TextTypes.bodyOneDemiText}
        />
        <TextWhj
          title={localisation.renewPlanDescriptionText}
          type={TextTypes.bodyThreeBookText}
        />
      </View>
    </ModalWhj>
  );
};

export default NoCreditLeftPopup;
