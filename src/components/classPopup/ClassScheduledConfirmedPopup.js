import React from 'react';
import {View, Image} from 'react-native';
import localisation from '../../localisation';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';
import { ClassPopupsImages } from '../../common/Images';
import { dateFormatter } from '../../utility';
import { DATE_FORMAT_ENUM } from '../../constants/enum';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import { SCREEN_HEIGHT } from '../../utility/SizeUtil';

const ClassScheduledConfirmedPopup = (props) => {
  const {selectedSlot,classSchedule} = props;

  return (
    <ModalWhj
      style={Styles.cancelledClassPopupStyle}
      scrollEnabled={true}
      setActionModalVisible={(value) => props.setActionModalVisible(value)}
      height={(SCREEN_HEIGHT * 30) / 100}
      isScrollIndicatorRequired={false}>
      <View style={Styles.classConfirmedPopupContianer}>
        <Image
          style={Styles.scheduleClassConfirmedEmojiStyle}
          source={ClassPopupsImages.classConfirmedEmoji}
        />
        <TextWhj
          title={classSchedule}
          type={TextTypes.bodyOneDemiText}
          style={Styles.yayClassEmojiTextScheduled}
        />
        <TextWhj
          title={localisation.classConfirmed(
            dateFormatter(
              selectedSlot?.startTime,
              DATE_FORMAT_ENUM.SLOT_TIMING_WITH_DAY_DATE_MONTH,
            ),
          )}
          type={TextTypes.bodyOneBookText}
          style={Styles.slotConfirmedStyle}
        />
      </View>
    </ModalWhj>
  );
};

ClassScheduledConfirmedPopup.propTypes = {};

export default ClassScheduledConfirmedPopup;
