import React from 'react';
import {View, Image} from 'react-native';
import Localisation from '../../localisation';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';
import { dateFormatter } from '../../utility';
import { ClassPopupsImages } from '../../common/Images';
import { DATE_FORMAT_ENUM } from '../../constants/enum';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import { SCREEN_HEIGHT } from '../../utility/SizeUtil';

const SlotCancelledConfirmation = (props) => {
  const {startTime} = props;

  return (
    <ModalWhj
      style={Styles.cancelledClassPopupStyle}
      scrollEnabled={true}
      setActionModalVisible={(value) => props.setActionModalVisible(value)}
      height={(SCREEN_HEIGHT * 30) / 100}
      isScrollIndicatorRequired={false}>
      <View style={Styles.classConfirmedPopupContianer}>
        <Image
          style={Styles.classConfirmedEmojiStyle}
          source={ClassPopupsImages.classCancelled}
        />
        <TextWhj
          title={Localisation.slotCancelled}
          type={TextTypes.bodyOneDemiText}
          style={Styles.yayClassScheduledStyle}
        />
        <TextWhj
          title={Localisation.slotCancelledConfirmed(
            dateFormatter(
              startTime,
              DATE_FORMAT_ENUM.SLOT_TIMING_WITH_DAY_DATE_MONTH,
            ),
          )}
          type={TextTypes.bodyOneBookText}
          style={Styles.slotConfirmedStyle}
        />
      </View>
    </ModalWhj>
  );
};

SlotCancelledConfirmation.propTypes = {};

export default SlotCancelledConfirmation;
