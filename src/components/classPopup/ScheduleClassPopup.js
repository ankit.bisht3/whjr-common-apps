import React, { useState } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import localisation from '../../localisation';
import Common from '../../localisation/enUS/Common';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';
import BookedClassDetails from './common/BookedClassDetails';
import { SCREEN_TYPE_BOOSTER_CLASS } from '../../constants/enum';
import Colors from '../../common/Colors';
import { BoosterImages } from '../../common/Images';
import OTPTextView from '../otp/OTPTextView';
import { SCREEN_HEIGHT } from '../../utility/SizeUtil';
import { OTP_TEXT_CONFIG, API_STATUS_CODES, COURSE_TYPE_STATUS } from '../../constants/enum';
import { validateToken, sendVerificationCode } from '../../actions/ClassesActions';
import { trimString } from '../../utility';

const UpcomingClassDetail = (props) => {
    const [validatingOTP, setValidateOtp] = useState(false);
    const [resendFlagEnable, setResendFlagEnable] = useState(false);
    const [resendFlag, setResendFlag] = useState(false);
    const [otp, setOtp] = useState('');
    const { mobile, email, isOtpOnCancellation } = props;
    const [value, setValue] = useState(
        props.screenType === SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER
            ? props.selectedCourseType == COURSE_TYPE_STATUS.MATH
                ? Common.worksheetBoosterClass
                : Common.projectBoosterClass
            : Common.regularClass
    );
    const [visible, setVisible] = useState(false);
    const [isParentalLock, CheckParentalLock] = useState(false);
    const [isRegularClass, checkRegularClass] = useState(false);
    const isLessThanSevenHun = SCREEN_HEIGHT < 700;

    const projectBoosterHandle = () => {
        if (props.selectedCourseType == COURSE_TYPE_STATUS.MATH) {
            setValue(Common.worksheetBoosterClass);
        } else {
            setValue(Common.projectBoosterClass);
        }
        setVisible(false);
    };

    const regularClassHandle = () => {
        setValue(Common.regularClass);
        setVisible(false);
    };

    const resetResendFlag = (status) => {
        setResendFlagEnable(status);
    };

    const resendOTPFlag = (status) => {
        setResendFlag(status);
    };

    const getDropDownStyle = () => {
        return [
            Styles.dropDownStyle,
            {
                width: Platform.OS === 'android' ? '91%' : '92%',
                top: value === Common.regularClass ? '33%' : '25%'
            }
        ]
    }

    const resendOTP = async () => {
        const response = await props.sendVerificationCode();
        if (response.status === API_STATUS_CODES.RESPONSE_200) {
            setResendFlag(true);
            setResendFlagEnable(false);
            setOtp('');
            setValidateOtp(false);
        }
    };

    const validateTkn = () => {
        props.validateToken(otp).then((res) => {
            if (res?.data?.success) {
                if (isRegularClass) {
                    props?.bookSlot();
                } else {
                    props?.bookUpcomingSlot(otp);
                }
            } else {
                setValidateOtp(true);
            }
        });
    };

    const handleTextChange = (field) => (text) => {
        setOtp(trimString(text));
        if (field === 'otp' && validatingOTP) {
            setValidateOtp(false);
        }
    };

    const handleScreenType = (screenType) => {
        return screenType;
    };

    const handleSelectedClassType = () => {
        handleScreenType(value);
        setVisible(!visible);
    };

    const scheduleClass = () => {
        if (value === Common.regularClass) {
            if (isOtpOnCancellation) {
                CheckParentalLock(true);
                checkRegularClass(true);
                props.sendVerificationCode();
            } else {
                props?.bookSlot();
            }
        } else {
            if (
                value === Common.projectBoosterClass ||
                value === Common.worksheetBoosterClass
            ) {
                CheckParentalLock(true);
                checkRegularClass(false);
                props.sendVerificationCode();
            }
        }
    };

    const bottomBar = () => {
        return (<View style={Styles.scheduleNowStyle}>
            {isParentalLock && (
                <>
                    <ButtonWhj
                        onPress={(btn) => props.setActionModalVisible(btn)}
                        type={ButtonTypes.plainGreyMediumButton}
                        title={localisation.back}
                        textColor={Colors.orgTextColor}
                    />
                    <ButtonWhj
                        onPress={() => validateTkn()}
                        title={'confirm'}
                        width={162}
                        style={Styles.boosterScheduleStyle}
                        disabled={otp.length === 4 ? false : true}
                        type={
                            otp.length === 4
                                ? ButtonTypes.purpleBigButton
                                : ButtonTypes.greyMediumButton
                        }
                    />
                </>
            )}
            {!isParentalLock && (
                <>
                    <ButtonWhj
                        onPress={(btn) => props.setActionModalVisible(btn)}
                        type={ButtonTypes.plainGreyMediumButton}
                        title={localisation.back}
                        textColor={Colors.orgTextColor}
                    />
                    <ButtonWhj
                        onPress={() => scheduleClass()}
                        title={localisation.scheduleNowClass}
                        width={162}
                        style={Styles.rightMostButtonStyle}
                        type={ButtonTypes.purpleBigButton}
                    />
                </>
            )}
        </View>)
    }

    const topHeader = (
        <View style={Styles.headerContainerStyle}>
            <View style={Styles.selectTeacherHeaderContainer}>
                <TextWhj
                    title={localisation.scheduleClassTitle}
                    type={TextTypes.bodyOneBlackText}
                />
            </View>
        </View>
    );

    return (
        <ModalWhj
            scrollEnabled={isLessThanSevenHun}
            setActionModalVisible={(status) => props.setActionModalVisible(status)}
            showsVerticalScrollIndicator={false}>
            {topHeader}
            <View style={Styles.classTypeContainer}>
                <View style={Styles.classTypeSubContainer}>
                    <View>
                        <TextWhj
                            title={Common.selectClassType}
                            type={TextTypes.bodyTwoDemiText}
                            style={Styles.classTypeHeadingText}
                        />
                        <TouchableOpacity
                            style={Styles.buttonStyle}
                            onPress={() => handleSelectedClassType()}>
                            <View style={Styles.boosterDropDownContainerStyle}>
                                <TextWhj
                                    title={value}
                                    type={TextTypes.bodyOneBookText}
                                    style={Styles.buttonTextStyle}
                                />
                                <Image
                                    source={BoosterImages.arrowDropDown}
                                    style={{ height: 7, width: 10 }}
                                />
                            </View>
                        </TouchableOpacity>
                        <View>
                            {value === Common.projectBoosterClass ||
                                value === Common.worksheetBoosterClass ? (
                                <View>
                                    <View style={{ marginTop: 12 }}>
                                        <TextWhj
                                            title={Common.notes}
                                            type={TextTypes.bodyThreeDemiText}
                                            style={Styles.noteText}
                                        />
                                    </View>
                                    <View style={{ marginTop: 4 }}>
                                        <TextWhj
                                            title={
                                                props.selectedCourseType == COURSE_TYPE_STATUS.MATH
                                                    ? Common.worksheetBoosterClassDesc
                                                    : Common.projectBoosterClassDesc
                                            }
                                            type={TextTypes.bodyThreeBookText}
                                            style={Styles.descTextStyle}
                                        />
                                    </View>
                                </View>
                            ) : null}
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ marginHorizontal: 16 }}>
                <BookedClassDetails
                    classData={props?.data}
                    isTeacherEmail={false}
                    isSubstitute={props.isSubstitute}
                    showDefaultTeacher={true}
                    onSelectTeacher={() => props.onSelectTeacher()}
                />
            </View>
            {visible ? (
                <View
                    style={getDropDownStyle()}>
                    {props?.projectBoosterStatus?.data?.PROJECT_BOOSTER?.promote ===
                        true ? (
                        <View
                            style={{
                                backgroundColor: Colors.whiteWhj,
                                height: 60,
                                justifyContent: 'center',
                            }}>
                            <TouchableOpacity
                                style={{
                                    backgroundColor:
                                        value === Common.projectBoosterClass ||
                                            value === Common.worksheetBoosterClass
                                            ? Colors.yellowLightWhj
                                            : Colors.whiteWhj,
                                    height: 50,
                                    borderRadius: 10,
                                    marginHorizontal: 10,
                                }}
                                onPress={projectBoosterHandle}>
                                <TextWhj
                                    title={
                                        props.selectedCourseType === COURSE_TYPE_STATUS.MATH
                                            ? Common.worksheetBoosterClass
                                            : Common.projectBoosterClass
                                    }
                                    type={TextTypes.bodyOneBookText}
                                    style={Styles.buttonTextStyle}
                                />
                            </TouchableOpacity>
                        </View>
                    ) : null}
                    <View
                        style={{
                            backgroundColor: Colors.whiteWhj,
                            height: 60,
                            justifyContent: 'center',
                        }}>
                        <TouchableOpacity
                            style={{
                                backgroundColor:
                                    value === Common.regularClass
                                        ? Colors.yellowLightWhj
                                        : Colors.whiteWhj,
                                height: 50,
                                borderRadius: 10,
                                marginHorizontal: 10,
                                marginBottom: 10,
                            }}
                            onPress={regularClassHandle}>
                            <TextWhj
                                title={Common.regularClass}
                                type={TextTypes.bodyOneBookText}
                                style={Styles.buttonTextStyle}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            ) : null}
            {isParentalLock && (
                <>
                    <View style={Styles.otpContainer}>
                        <TextWhj
                            title={localisation.youHaveSentOTP}
                            style={Styles.otpDetailsContainer}
                            type={TextTypes.bodyTwoBookText}>
                            {mobile} {localisation.youHaveSentOTPAnd} {email}
                            {'.'}
                        </TextWhj>
                        <View>
                            <OTPTextView
                                autoFocusOnLoad
                                resendFlag={resendFlag}
                                validatingOTP={validatingOTP}
                                resendOTPFlag={(value) => resendOTPFlag(value)}
                                resendCancelClassOTP={() => resendOTP()}
                                isClassCancel
                                otpTime={OTP_TEXT_CONFIG.OTP_VALIDITY}
                                containerStyle={Styles.cancleClassOTPBottomMargin(
                                    validatingOTP ? 0 : 8,
                                )}
                                handleTextChange={handleTextChange('otp')}
                            />
                            {validatingOTP && (
                                <TextWhj
                                    title={localisation.otpInvalidText}
                                    style={Styles.cancelClassInvalidStyle}
                                    type={TextTypes.bodyTwoDemiText}
                                />
                            )}
                        </View>
                    </View>
                </>
            )}
            {(value === Common.projectBoosterClass || value === Common.worksheetBoosterClass) ? (
                <View style={Styles.creditTextContainer}>
                    <TextWhj
                        title={localisation.oneCredit}
                        type={TextTypes.bodyTwoMediumText}
                        style={Styles.lose1CreditTextStyle}>
                        {' '}
                        <TextWhj
                            title={localisation.creditConsumedMsg}
                            type={TextTypes.bodyTwoMediumBlackText}
                            style={Styles.losseCreditStyle}
                        />
                    </TextWhj>
                </View>
            ) : null}
            {bottomBar()}
        </ModalWhj>
    );
};
UpcomingClassDetail.propTypes = {};

// const mapStateToProps = (state) => {
//     return {
//         mobile: state?.userInfo?.userInfo?.mobile,
//         email: state?.userInfo?.userInfo?.email,
//         timeZone: state?.userInfo?.userInfo?.timezone,
//         projectBoosterStatus: state?.BoosterClassReducer,
//         isOtpOnCancellation: state?.userInfo?.userInfo?.isOtpOnCancellation,
//     };
// };

// export default connect(mapStateToProps, {
//     validateToken,
//     sendVerificationCode,
// })(UpcomingClassDetail);

export default UpcomingClassDetail;

