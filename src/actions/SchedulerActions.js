import { getAvaiableTeacherList, getOneTimeScheduler } from "../apiClient/ApiCalls";
import { scheduleSlotParsingModel } from "../apiClient/model/ScheduleSlotParsingModel";
import { teacherModel } from "../apiClient/model/TeacherModel";

export const getOneTimeSchedulerSlots = async (studentId, teacherId) => {
    try {
        console.log("studentId, teacherId", studentId, teacherId)
        const response = await getOneTimeScheduler(studentId, {
            teacherId: teacherId,
        });
        //   const timezone = getState().userInfo.userInfo.timezone;
        const timezone = "Asia/Kolkata";
        console.log("responseresponse", response)
        let payload;
        if (response && response?.data && response?.data?.data) {
            payload = scheduleSlotParsingModel(response?.data?.data, timezone);
        }
        // console.log("responseresponse", response, payload)

        return payload;
    } catch (error) {
        //   dispatch({
        //     type: ActionTypes.GET_ONE_TIME_SCHEDULER_REQUEST_FAILURE,
        //     payload: error,
        //   });
        console.error(error);
    }
};

export const getSubstituteTeacherForOneTimeScheduler = async (studentId) => {
    try {
        const teacherList = await getAvaiableTeacherList(studentId);
        const payload = teacherList.data?.data?.map((teacher) => {
            return teacherModel(teacher);
        });
        return payload
    } catch (error) {
        //   dispatch({
        //     type: ActionTypes.GET_TEACHER_LIST_REQUEST_FAILURE,
        //     payload: error,
        //   });
        console.error(error);
    }
};