import { authenticateCancelClassToken } from "../apiClient/ApiCalls";

export const validateToken = (otpToken) => async (dispatch, getState) => {
    try {
        const emailOrMobile = "+918700387636";
        const params = { emailOrMobile, token: otpToken };
        const response = await authenticateCancelClassToken(params);
        return response;
    } catch (error) { }
};

export const sendVerificationCode = () => async (dispatch, getState) => {
    try {
        const emailOrMobile = "8700387636";
        const dialCode = "+91";
        const tokenType = 'parental_otp';
        const params = { emailOrMobile, dialCode, tokenType };
        const response = await sendCancelVerificationCode(params);
        return response;
    } catch (error) { }
};