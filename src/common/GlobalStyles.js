import { StyleSheet } from 'react-native';
import { isIOS } from '../utility';
import Colors from './Colors';

export const GlobalStyles = StyleSheet.create({
    separatorView: {
        marginVertical: 8,
        backgroundColor: Colors.separatorWhj,
        borderColor: Colors.separatorWhj,
        borderWidth: 1,
    },
    loginInputCardView: isIOS() ? iosInputShadow : androidInputShadow
});

const iosInputShadow = {
    backgroundColor: Colors.whiteWhj,
    shadowRadius: 10,
    shadowOpacity: 0.05,
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowColor: Colors.blackWhj,
};

const androidInputShadow = {
    backgroundColor: Colors.whiteWhj,
};


export const androidOtpInputBoxShadow = {
    height: 50,
    width: 50,
    x: 0,
    y: 2,
    border: 10,
    opacity: 0.025,
    color: Colors.blackTextWhj,
};
