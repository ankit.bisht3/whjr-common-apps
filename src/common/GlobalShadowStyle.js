import {StyleSheet} from 'react-native';
import Colors from './Colors';

export const cardWithShadow = (
  width,
  height,
  shadowColor,
  opacity,
  x,
  y,
  border,
) => {
  return {
    height: height,
    width: width,
    color: shadowColor ? shadowColor : Colors.blackTextWhj,
    border: border ? border : 10,
    opacity: opacity ? opacity : Colors.shaodwOpacity,
    x: x ? x : 0,
    y: y ? y : 5,
  };
};

export const GlobalShadowStyles = StyleSheet.create({
  cardWithShadowAndBorderIOS: {
    backgroundColor: Colors.whiteWhj,
    borderRadius: 5,
    shadowRadius: 5,
    shadowOpacity: Colors.shaodwOpacity,
    shadowOffset: {
      height: 2,
    },
    shadowColor: 'rgb(240,240,240)',
  },
  boxShadowStyle: {
    alignSelf: 'center',
    position: 'absolute',
    backgroundColor: Colors.whiteWhj,
    top: 10,
    bottom: 10,
    right: 10,
    left: 10,
    borderWidth: 1,
    borderRadius: 10,
    overflow: 'hidden',
  },
});
