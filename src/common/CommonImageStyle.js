import { StyleSheet } from 'react-native';

const CommonImageStyle = StyleSheet.create({
  lockIcon: {
    margin: 13,
    height: 22,
    width: 16,
    resizeMode: 'contain',
  },
});

export default CommonImageStyle;
