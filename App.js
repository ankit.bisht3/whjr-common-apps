import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import ApiClient from './src/apiClient';
import { COURSE_TYPE_STATUS } from './src/constants/enum';
import PaidScheduler from './src/views/screens/Scheduler/PaidScheduler'
const apiClient = new ApiClient();
apiClient.creatAxiosInstance();

const App = () => {

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <PaidScheduler
        selectedCourseType={COURSE_TYPE_STATUS.MATH}
        teacherId={'bf955657-1d23-4477-b9cf-7eee2d39435f'}/>
    </SafeAreaView>
  );
};


export default App;
